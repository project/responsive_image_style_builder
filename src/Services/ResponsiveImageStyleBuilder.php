<?php

namespace Drupal\responsive_image_style_builder\Services;

use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\DependencyInjection\Container;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;

class ResponsiveImageStyleBuilder
{
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager
  ) {}

  public static function create(Container $container): self
  {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  public function getImageStyleOptions(): array
  {
    $options = ['' => t('None')];
    $storage = $this->entityTypeManager->getStorage('image_style');
    $imageStyles = $storage->loadMultiple();

    foreach ($imageStyles as $imageStyleId => $imageStyle) {
      $options[$imageStyleId] = $imageStyle->label();
    }

    return $options;
  }

  public function createFallbackImageStyle(ResponsiveImageStyle $responsiveImageStyle): ImageStyle
  {
    $responsiveImageStyleId = sprintf('%s_default', $responsiveImageStyle->id());
    return $this->createImageStyle(['id' => $responsiveImageStyleId, 'label' => $responsiveImageStyle->label()]);
  }

  /**
   * Fetches an image style by given properties.
   * In case there's a new image style to be created, the label is automatically set.
   *
   * @param array $imageStyle
   * @return ImageStyle
   */
  private function createImageStyle(array|string $imageStyleConfig): ImageStyle
  {
    if (is_string($imageStyleConfig)) {
      $imageStyleConfig = [
        'id' => $this->getMachineName($imageStyleConfig),
        'label' => $imageStyleConfig
      ];
    }

    $storage = $this->entityTypeManager->getStorage('image_style');
    $imageStyle = $storage->load($imageStyleConfig['id']);

    if (!$imageStyle) {
      $imageStyle = ImageStyle::create([
        'name' => $imageStyleConfig['id'],
        'label' => $imageStyleConfig['label'] ?? $imageStyleConfig['id']
      ]);

      foreach ($this->getDefaultEffects($imageStyle->id()) as $imageEffect) {
        $imageStyle->addImageEffect($imageEffect);
      }
      $imageStyle->save();
    }
    return $imageStyle;
  }

  public function getImageStyle(string $imageStyleId): ?ImageStyle
  {
    $storage = $this->entityTypeManager->getStorage('image_style');
    return $storage->load($imageStyleId);
  }

  public function createDerivativeImageStyle(array $imageStyleConfig): ImageStyle
  {
    return $this->createImageStyle($imageStyleConfig);
  }

  public function getDerivativeImageStyles(string $parentImageStyleId): array
  {
    $derivativeImageStyles = [];
    $styles = ImageStyle::loadMultiple();
    $imageStyleBaseId = substr($parentImageStyleId, 0, strrpos($parentImageStyleId, '_'));;

    foreach ($styles as $imageStyleId => $imageStyle) {
      // Skip the parent
      if ($imageStyleId == $parentImageStyleId) {
        continue;
      }
      if ($this->getImageStyleBaseId($imageStyleId) == $imageStyleBaseId) {
        $derivativeImageStyles[$imageStyleId] = $imageStyle;
      }
    }

    return $derivativeImageStyles;
  }

  private function getImageStyleBaseId(string $imageStyleId): string
  {
    $stripMultiplier = substr($imageStyleId, 0, strrpos($imageStyleId, '_'));
    $imageStyleBaseId = substr($stripMultiplier, 0, strrpos($stripMultiplier, '_'));

    return $imageStyleBaseId;
  }

  /**
   * Returns default effects, or if an id is given, the effects of the parent.
   *
   * @param string $imageStyleId
   * @return array
   */
  private function getDefaultEffects(string $imageStyleId = ''): array
  {
    $i = 0;
    $effects = [];

    // Check if there's a "parent" image style
    $baseImageStyleId = $this->getImageStyleBaseId($imageStyleId);

    if (!empty($baseImageStyleId)) {
      /** @var ImageStyle $parentImageStyle */
      $parentImageStyle = \Drupal::service('risb.builder')->getImageStyle($baseImageStyleId . '_default');

      if ($parentImageStyle) {
        foreach ($parentImageStyle->getEffects() as $effect) {
          $effects[] = $effect->getConfiguration();
        }
        return $effects;
      }
    }

    // @todo Make this variable overrideable
    $configurations = [
      'image_scale_and_crop' => [
        'width' => 100,
        'height' => 100
      ],
      'image_convert' => [
        'format' => 'webp',
        'quality' => 100,
      ],
    ];

    foreach ($configurations as $configurationId => $data) {
      $configuration = [
        'uuid' => NULL,
        'id' => $configurationId,
        'weight' => $i * 10,
        'data' => $data,
      ];
      $effect = \Drupal::service('plugin.manager.image.effect')->createInstance($configuration['id'], $configuration);
      $effects[] = $effect->getConfiguration();
      $i += 1;
    }

    return $effects;
  }

  private function getMachineName($string): string
  {
    $transliterated = \Drupal::transliteration()->transliterate($string, LanguageInterface::LANGCODE_DEFAULT, '_');
    $transliterated = mb_strtolower($transliterated);
    $transliterated = preg_replace('@[^a-z0-9_.]+@', '_', $transliterated);
    return $transliterated;
  }
}
